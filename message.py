#!/usr/bin/env python3
import time
#replaced by the following two lines for testing purposes
#import falsifytime
#time = falsifytime.falsifytime()

class Message:
	#the constructor can take various parameters by default generates an "I said" message
	#but it must handle "covid patients said" and "I heard" messages as well
	#you specify the constructor…
	def __init__(self):

	#a class method that generates a random message (to be called for I said messages)
	@classmethod
	def generate(self):

	#a method to convert the object to string data
	def __str__(self):

	#export/import
	def m_export(self):

	def m_import(self, msg):

#will only execute if this file is run
if __name__ == "__main__":
	#test the class
	myMessage = Message()
	time.sleep(1)
	mySecondMessage = Message()#I heard message
	copyOfM = Message()
	copyOfM.m_import(myMessage.m_export())
	print(myMessage)
	print(mySecondMessage)
	print(copyOfM)
